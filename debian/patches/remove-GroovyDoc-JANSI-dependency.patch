From: Markus Koschany <apo@debian.org>
Date: Sat, 25 Jun 2022 21:45:46 +0200
Subject: remove GroovyDoc JANSI dependency

Origin: https://github.com/apache/groovy/commit/05c5e003d79b9a5d10679db7800036184b1047f6
Forwarded: not-needed
---
 src/main/org/codehaus/groovy/tools/shell/IO.java   | 80 +++++++++++++++++-----
 .../codehaus/groovy/tools/shell/util/Logger.java   | 23 +++++--
 2 files changed, 78 insertions(+), 25 deletions(-)

diff --git a/src/main/org/codehaus/groovy/tools/shell/IO.java b/src/main/org/codehaus/groovy/tools/shell/IO.java
index 18bd8c2..123fb46 100644
--- a/src/main/org/codehaus/groovy/tools/shell/IO.java
+++ b/src/main/org/codehaus/groovy/tools/shell/IO.java
@@ -18,8 +18,8 @@
  */
 package org.codehaus.groovy.tools.shell;
 
+import org.codehaus.groovy.runtime.InvokerHelper;
 import org.codehaus.groovy.tools.shell.util.Preferences;
-import org.fusesource.jansi.AnsiRenderWriter;
 
 import java.io.Closeable;
 import java.io.IOException;
@@ -34,26 +34,44 @@ import java.io.Reader;
  *
  * @author <a href="mailto:jason@planet57.com">Jason Dillon</a>
  */
-public class IO implements Closeable
-{
-    /** Raw input stream. */
+public class IO implements Closeable {
+    private static final String ANSI_RENDER_WRITER = "org.fusesource.jansi.AnsiRenderWriter";
+
+    /**
+     * Raw input stream.
+     */
     public final InputStream inputStream;
 
-    /** Raw output stream. */
+    /**
+     * Raw output stream.
+     */
     public final OutputStream outputStream;
 
-    /** Raw error output stream. */
+    /**
+     * Raw error output stream.
+     */
     public final OutputStream errorStream;
 
-    /** Prefered input reader. */
+    /**
+     * Preferred input reader.
+     */
     public final Reader in;
 
-    /** Prefered output writer. */
+    /**
+     * Preferred output writer.
+     */
     public final PrintWriter out;
 
-    /** Prefered error output writer. */
+    /**
+     * Preferred error output writer.
+     */
     public final PrintWriter err;
 
+    /**
+     * Whether ansi support is available
+     */
+    public final boolean ansiSupported;
+
     /**
      * Construct a new IO container.
      */
@@ -67,8 +85,36 @@ public class IO implements Closeable
         this.errorStream = errorStream;
 
         this.in = new InputStreamReader(inputStream);
-        this.out = new AnsiRenderWriter(outputStream, true);
-        this.err = new AnsiRenderWriter(errorStream, true);
+        boolean ansiSupported = false;
+        try {
+            Class.forName(ANSI_RENDER_WRITER, false, IO.class.getClassLoader());
+            ansiSupported = true;
+        } catch (ClassNotFoundException ignore) {
+        }
+        this.ansiSupported = ansiSupported;
+        PrintWriter out = null;
+        PrintWriter err = null;
+        if (ansiSupported) {
+            out = tryConstructRenderWriter(outputStream);
+            err = tryConstructRenderWriter(errorStream);
+        }
+        if (out == null) {
+            out = new PrintWriter(outputStream, true);
+        }
+        if (err == null) {
+            err = new PrintWriter(errorStream, true);
+        }
+        this.out = out;
+        this.err = err;
+    }
+
+    protected PrintWriter tryConstructRenderWriter(OutputStream stream) {
+        // load via reflection to avoid hard-coded dependency on jansi jar
+        try {
+            return (PrintWriter) InvokerHelper.invokeConstructorOf(ANSI_RENDER_WRITER, new Object[]{stream, true});
+        } catch (ClassNotFoundException ignore) {
+            return null;
+        }
     }
 
     /**
@@ -80,8 +126,6 @@ public class IO implements Closeable
 
     /**
      * Set the verbosity level.
-     *
-     * @param verbosity
      */
     public void setVerbosity(final Verbosity verbosity) {
         assert verbosity != null;
@@ -144,12 +188,10 @@ public class IO implements Closeable
         err.close();
     }
 
-    //
-    // Verbosity
-    //
-
-    public static final class Verbosity
-    {
+    /**
+     * Verbosity for simple logging: QUIET, INFO, VERBOSE, DEBUG
+     */
+    public static final class Verbosity {
         public static final Verbosity QUIET = new Verbosity("QUIET");
 
         public static final Verbosity INFO = new Verbosity("INFO");
diff --git a/src/main/org/codehaus/groovy/tools/shell/util/Logger.java b/src/main/org/codehaus/groovy/tools/shell/util/Logger.java
index 30cd838..d20e2a7 100644
--- a/src/main/org/codehaus/groovy/tools/shell/util/Logger.java
+++ b/src/main/org/codehaus/groovy/tools/shell/util/Logger.java
@@ -60,20 +60,31 @@ public final class Logger {
             }
         }
 
-        Color color = GREEN;
-        if (WARN.equals(level) || ERROR.equals(level)) {
-            color = RED;
+        if (io.ansiSupported) {
+            logWithAnsi(level, msg);
+        } else {
+            logDefault(level, msg);
         }
 
-        io.out.println(ansi().a(INTENSITY_BOLD).fg(color).a(level).reset().a(" [").a(name).a("] ").a(msg));
-
         if (cause != null) {
             cause.printStackTrace(io.out);
         }
 
         io.flush();
     }
-    
+
+    private void logDefault(String level, Object msg) {
+        io.out.println(level + " [" + name + "] " + msg);
+    }
+
+    private void logWithAnsi(String level, Object msg) {
+        Color color = GREEN;
+        if (WARN.equals(level) || ERROR.equals(level)) {
+            color = RED;
+        }
+        io.out.println(ansi().a(INTENSITY_BOLD).fg(color).a(level).reset().a(" [").a(name).a("] ").a(msg));
+    }
+
     //
     // Level helpers
     //
