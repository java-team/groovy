From: Markus Koschany <apo@debian.org>
Date: Sat, 25 Jun 2022 22:34:11 +0200
Subject: update to JLine2 2.13

Remove JAnsiHelper and other obsolete code

Forwarded: not-needed
---
 build.gradle                                       |   2 +-
 .../tools/shell/InteractiveShellRunner.groovy      |  14 ++-
 .../PatchedCandidateListCompletionHandler.groovy   | 112 ---------------------
 .../groovy/tools/shell/PatchedConsoleReader.groovy | 107 --------------------
 .../groovy/tools/shell/util/JAnsiHelper.groovy     |  42 --------
 .../groovy/tools/shell/ErrorDisplayTest.groovy     |   4 +
 .../groovy/tools/shell/GroovyshTest.groovy         |  23 ++++-
 .../groovy/tools/shell/ShellRunnerTest.groovy      |  12 ++-
 .../tools/shell/ShellRunnerTestSupport.groovy      |   1 +
 9 files changed, 47 insertions(+), 270 deletions(-)
 delete mode 100644 subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/PatchedCandidateListCompletionHandler.groovy
 delete mode 100644 subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/PatchedConsoleReader.groovy
 delete mode 100644 subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/util/JAnsiHelper.groovy

diff --git a/build.gradle b/build.gradle
index 9db2168..5910679 100644
--- a/build.gradle
+++ b/build.gradle
@@ -135,7 +135,7 @@ ext {
     ivyVersion = '2.4.0'
     jansiVersion = '1.11'
     jarjarVersion = '1.3'
-    jlineVersion = '2.12'
+    jlineVersion = '2.13'
     jmockVersion = '1.2.0'
     logbackVersion = '1.1.2'
     log4jVersion = '1.2.17'
diff --git a/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/InteractiveShellRunner.groovy b/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/InteractiveShellRunner.groovy
index d73f80e..599c63f 100644
--- a/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/InteractiveShellRunner.groovy
+++ b/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/InteractiveShellRunner.groovy
@@ -20,6 +20,8 @@ package org.codehaus.groovy.tools.shell
 
 import jline.console.ConsoleReader
 import jline.console.completer.AggregateCompleter
+import jline.console.completer.CandidateListCompletionHandler
+import jline.console.completer.CompletionHandler
 import jline.console.history.FileHistory
 import org.codehaus.groovy.tools.shell.completion.CustomClassSyntaxCompletor
 import org.codehaus.groovy.tools.shell.completion.FileNameCompleter
@@ -53,8 +55,16 @@ class InteractiveShellRunner
 
         this.prompt = prompt
         this.wrappedInputStream = new WrappedInputStream(shell.io.inputStream)
-        this.reader = new PatchedConsoleReader(wrappedInputStream, shell.io.outputStream)
-        this.reader.setCompletionHandler(new PatchedCandidateListCompletionHandler())
+        this.reader = new ConsoleReader(wrappedInputStream, shell.io.outputStream)
+
+        CompletionHandler currentCompletionHandler = this.reader.getCompletionHandler()
+        if (currentCompletionHandler instanceof CandidateListCompletionHandler) {
+            // have to downcast because methods not part of the interface
+            ((CandidateListCompletionHandler) currentCompletionHandler).setStripAnsi(true)
+            ((CandidateListCompletionHandler) currentCompletionHandler).setPrintSpaceAfterFullCompletion(false)
+        }
+
+
         // expand events ia an advanced feature of JLine that clashes with Groovy syntax (e.g. invoke "2!=3")
         this.reader.expandEvents = false
 
diff --git a/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/PatchedCandidateListCompletionHandler.groovy b/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/PatchedCandidateListCompletionHandler.groovy
deleted file mode 100644
index 3fbaea5..0000000
--- a/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/PatchedCandidateListCompletionHandler.groovy
+++ /dev/null
@@ -1,112 +0,0 @@
-/*
- *  Licensed to the Apache Software Foundation (ASF) under one
- *  or more contributor license agreements.  See the NOTICE file
- *  distributed with this work for additional information
- *  regarding copyright ownership.  The ASF licenses this file
- *  to you under the Apache License, Version 2.0 (the
- *  "License"); you may not use this file except in compliance
- *  with the License.  You may obtain a copy of the License at
- *
- *    http://www.apache.org/licenses/LICENSE-2.0
- *
- *  Unless required by applicable law or agreed to in writing,
- *  software distributed under the License is distributed on an
- *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
- *  KIND, either express or implied.  See the License for the
- *  specific language governing permissions and limitations
- *  under the License.
- */
-package org.codehaus.groovy.tools.shell
-
-import groovy.transform.CompileStatic
-import jline.console.ConsoleReader
-import jline.console.CursorBuffer
-import jline.console.completer.CandidateListCompletionHandler
-import org.codehaus.groovy.tools.shell.util.JAnsiHelper
-
-/**
- * jline completion handler displays ANSIfied candidates nicely,
- * but does not de-ANSIfy when adding to the prompt :-(
- *
- * So this class just adds this functionality.
- *
- * See https://github.com/jline/jline2/issues/132
- */
-@CompileStatic
-class PatchedCandidateListCompletionHandler extends CandidateListCompletionHandler {
-
-    public boolean complete(final ConsoleReader reader, final List<CharSequence> candidates, final int pos) throws
-            IOException
-    {
-        CursorBuffer buf = reader.getCursorBuffer();
-        final List<CharSequence> deAnsifiedcandidates = candidates.collect({CharSequence candidate -> JAnsiHelper.stripAnsi(candidate) })
-
-        // if there is only one completion, then fill in the buffer
-        if (candidates.size() == 1) {
-            CharSequence value = deAnsifiedcandidates.get(0);
-
-            // fail if the only candidate is the same as the current buffer
-            if (value.equals(buf.toString())) {
-                return false;
-            }
-
-            setBuffer(reader, value, pos);
-
-            return true;
-        }
-        else if (candidates.size() > 1) {
-            String value = this.getUnambiguousCompletions(deAnsifiedcandidates);
-            setBuffer(reader, value, pos);
-        }
-
-        printCandidates(reader, candidates);
-
-        // redraw the current console buffer
-        reader.drawLine();
-
-        return true;
-    }
-
-    /**
-     * copied from CandidateListCompletionHandler because it was private :-(
-     * Returns a root that matches all the {@link String} elements of the specified {@link List},
-     * or null if there are no commonalities. For example, if the list contains
-     * <i>foobar</i>, <i>foobaz</i>, <i>foobuz</i>, the method will return <i>foob</i>.
-     */
-    private String getUnambiguousCompletions(final List<CharSequence> candidates) {
-        if (candidates == null || candidates.isEmpty()) {
-            return null;
-        }
-
-        // convert to an array for speed
-        String[] strings = candidates.toArray(new String[candidates.size()]);
-
-        String first = strings[0];
-        StringBuilder candidate = new StringBuilder();
-
-        for (int i = 0; i < first.length(); i++) {
-            if (startsWith(first.substring(0, i + 1), strings)) {
-                candidate.append(first.charAt(i));
-            }
-            else {
-                break;
-            }
-        }
-
-        return candidate.toString();
-    }
-
-    /**
-     * copied from CandidateListCompletionHandler because it was private :-(
-     * @return true is all the elements of <i>candidates</i> start with <i>starts</i>
-     */
-    private boolean startsWith(final String starts, final String[] candidates) {
-        for (String candidate : candidates) {
-            if (!candidate.startsWith(starts)) {
-                return false;
-            }
-        }
-
-        return true;
-    }
-}
diff --git a/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/PatchedConsoleReader.groovy b/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/PatchedConsoleReader.groovy
deleted file mode 100644
index bcad8d4..0000000
--- a/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/PatchedConsoleReader.groovy
+++ /dev/null
@@ -1,107 +0,0 @@
-/*
- *  Licensed to the Apache Software Foundation (ASF) under one
- *  or more contributor license agreements.  See the NOTICE file
- *  distributed with this work for additional information
- *  regarding copyright ownership.  The ASF licenses this file
- *  to you under the Apache License, Version 2.0 (the
- *  "License"); you may not use this file except in compliance
- *  with the License.  You may obtain a copy of the License at
- *
- *    http://www.apache.org/licenses/LICENSE-2.0
- *
- *  Unless required by applicable law or agreed to in writing,
- *  software distributed under the License is distributed on an
- *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
- *  KIND, either express or implied.  See the License for the
- *  specific language governing permissions and limitations
- *  under the License.
- */
-package org.codehaus.groovy.tools.shell
-
-import groovy.transform.CompileStatic
-import jline.console.ConsoleReader
-import jline.internal.Log
-import org.codehaus.groovy.tools.shell.util.JAnsiHelper
-
-@CompileStatic
-class PatchedConsoleReader extends ConsoleReader {
-
-
-    public PatchedConsoleReader(final InputStream inStream, final OutputStream out) throws IOException {
-        super(inStream, out);
-    }
-
-    /**
-     * copied from jline2.0 and modified to invoke stripAnsi() for length calculations
-     * Output the specified {@link Collection} in proper columns.
-     * See https://github.com/jline/jline2/issues/132
-     */
-    public void printColumns(final Collection<? extends CharSequence> items) throws IOException {
-        if (items == null || items.isEmpty()) {
-            return;
-        }
-
-        int width = getTerminal().getWidth();
-        int height = getTerminal().getHeight();
-
-        int maxWidth = 0;
-        for (CharSequence item : items) {
-            maxWidth = Math.max(maxWidth, JAnsiHelper.stripAnsi(item).length());
-        }
-        maxWidth = maxWidth + 3;
-        Log.debug("Max width: ", maxWidth);
-
-        int showLines;
-        if (isPaginationEnabled()) {
-            showLines = height - 1; // page limit
-        }
-        else {
-            showLines = Integer.MAX_VALUE;
-        }
-
-        StringBuilder buff = new StringBuilder();
-        int realLength = 0;
-        for (CharSequence item : items) {
-            if ((realLength + maxWidth) > width) {
-                println(buff);
-                buff.setLength(0);
-                realLength = 0;
-
-                if (--showLines == 0) {
-                    // Overflow
-                    print(resources.getString("DISPLAY_MORE"));
-                    flush();
-                    int c = readCharacter();
-                    if (c == '\r' || c == '\n') {
-                        // one step forward
-                        showLines = 1;
-                    }
-                    else if (c != 'q') {
-                        // page forward
-                        showLines = height - 1;
-                    }
-
-                    back(resources.getString("DISPLAY_MORE").length());
-                    if (c == 'q') {
-                        // cancel
-                        break;
-                    }
-                }
-            }
-
-            // NOTE: toString() is important here due to AnsiString being retarded
-            buff.append(item.toString());
-            int strippedItemLength = JAnsiHelper.stripAnsi(item).length()
-            realLength += strippedItemLength
-            for (int i = 0; i < (maxWidth - strippedItemLength); i++) {
-                buff.append(' ');
-            }
-            realLength += maxWidth - strippedItemLength;
-        }
-
-        if (buff.length() > 0) {
-            println(buff);
-        }
-    }
-
-}
diff --git a/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/util/JAnsiHelper.groovy b/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/util/JAnsiHelper.groovy
deleted file mode 100644
index 307731b..0000000
--- a/subprojects/groovy-groovysh/src/main/groovy/org/codehaus/groovy/tools/shell/util/JAnsiHelper.groovy
+++ /dev/null
@@ -1,42 +0,0 @@
-/*
- *  Licensed to the Apache Software Foundation (ASF) under one
- *  or more contributor license agreements.  See the NOTICE file
- *  distributed with this work for additional information
- *  regarding copyright ownership.  The ASF licenses this file
- *  to you under the Apache License, Version 2.0 (the
- *  "License"); you may not use this file except in compliance
- *  with the License.  You may obtain a copy of the License at
- *
- *    http://www.apache.org/licenses/LICENSE-2.0
- *
- *  Unless required by applicable law or agreed to in writing,
- *  software distributed under the License is distributed on an
- *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
- *  KIND, either express or implied.  See the License for the
- *  specific language governing permissions and limitations
- *  under the License.
- */
-package org.codehaus.groovy.tools.shell.util
-
-import groovy.transform.CompileStatic
-import org.fusesource.jansi.AnsiOutputStream
-
-@CompileStatic
-class JAnsiHelper {
-
-    /**
-     * copied from jline2 ConsoleReader
-     */
-    static CharSequence stripAnsi(final CharSequence str) {
-        if (str == null) return ''
-        try {
-            ByteArrayOutputStream baos = new ByteArrayOutputStream()
-            AnsiOutputStream aos = new AnsiOutputStream(baos)
-            aos.write(str.toString().bytes)
-            aos.flush()
-            return baos.toString()
-        } catch (IOException e) {
-            return str
-        }
-    }
-}
diff --git a/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ErrorDisplayTest.groovy b/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ErrorDisplayTest.groovy
index 87e18e7..6245048 100644
--- a/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ErrorDisplayTest.groovy
+++ b/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ErrorDisplayTest.groovy
@@ -19,12 +19,14 @@
 package org.codehaus.groovy.tools.shell
 
 import jline.console.ConsoleReader
+import jline.console.completer.CandidateListCompletionHandler
 
 
 class ErrorDisplayTest extends ShellRunnerTestSupport {
 
     void testInput() {
         readerStubber.demand.readLine { 'foo' }
+        readerStubber.demand.getCompletionHandler {new CandidateListCompletionHandler()}
         shellMocker.use {
             readerStubber.use {
                 Groovysh shellMock = new Groovysh()
@@ -40,6 +42,7 @@ class ErrorDisplayTest extends ShellRunnerTestSupport {
 
     void testError() {
         readerStubber.demand.readLine { throw new StringIndexOutOfBoundsException() }
+        readerStubber.demand.getCompletionHandler {new CandidateListCompletionHandler()}
         shellMocker.use {
             readerStubber.use {
                 Groovysh shellMock = new Groovysh()
@@ -55,6 +58,7 @@ class ErrorDisplayTest extends ShellRunnerTestSupport {
 
     void testError2() {
         readerStubber.demand.readLine { throw new Throwable('MockException') }
+        readerStubber.demand.getCompletionHandler {new CandidateListCompletionHandler()}
         shellMocker.use { readerStubber.use {
             Groovysh shellMock = new Groovysh()
             ConsoleReader readerStub = new ConsoleReader()
diff --git a/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/GroovyshTest.groovy b/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/GroovyshTest.groovy
index 7791827..f7da094 100644
--- a/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/GroovyshTest.groovy
+++ b/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/GroovyshTest.groovy
@@ -23,8 +23,8 @@ import org.codehaus.groovy.control.MultipleCompilationErrorsException
 import org.codehaus.groovy.tools.shell.completion.ReflectionCompletionCandidate
 import org.codehaus.groovy.tools.shell.completion.ReflectionCompletor
 import org.codehaus.groovy.tools.shell.completion.TokenUtilTest
-import org.codehaus.groovy.tools.shell.util.JAnsiHelper
 import org.codehaus.groovy.tools.shell.util.Preferences
+import org.fusesource.jansi.AnsiOutputStream
 
 class GroovyshTest extends GroovyTestCase {
 
@@ -503,10 +503,29 @@ ReflectionCompletor.getPublicFieldsAndMethods(new Foo(), '')
         def candidates = []
         compl.complete(TokenUtilTest.tokenList(/['a':3, 'b':4]./), candidates)
         assert candidates.size() > 1
-        assert candidates.reverse().subList(0, 3).collect({ String it -> JAnsiHelper.stripAnsi(it) }) == ['empty', 'b', 'a']
+        assert candidates.reverse().subList(0, 3).collect({ String it -> stripAnsi(it) }) == ['empty', 'b', 'a']
     }
+
+    /**
+    * copied from jline2 ConsoleReader
+    */
+    private static CharSequence stripAnsi(final CharSequence str) {
+        if (str == null) return ''
+        try {
+            ByteArrayOutputStream baos = new ByteArrayOutputStream()
+            AnsiOutputStream aos = new AnsiOutputStream(baos)
+            aos.write(str.toString().bytes)
+            aos.flush()
+            return baos.toString()
+        } catch (IOException e) {
+            return str
+        }
+    }
+
 }
 
+
+
 class GroovyshUtilsTest extends GroovyTestCase {
 
     void testIsTypeOrMethodDeclaration() {
diff --git a/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ShellRunnerTest.groovy b/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ShellRunnerTest.groovy
index 86e3518..41715d8 100644
--- a/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ShellRunnerTest.groovy
+++ b/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ShellRunnerTest.groovy
@@ -19,6 +19,8 @@
 package org.codehaus.groovy.tools.shell
 
 import groovy.mock.interceptor.MockFor
+import jline.console.ConsoleReader
+import jline.console.completer.CandidateListCompletionHandler
 import org.codehaus.groovy.tools.shell.util.Preferences
 
 class ShellRunnerTest extends GroovyTestCase {
@@ -94,8 +96,10 @@ class ShellRunnerTest extends GroovyTestCase {
     }
 
     private MockFor primedMockForConsoleReader() {
-        def readerMocker = new MockFor(PatchedConsoleReader)
-        readerMocker.demand.setCompletionHandler {}
+        def readerMocker = new MockFor(ConsoleReader)
+        CandidateListCompletionHandler clch = new CandidateListCompletionHandler()
+        clch.stripAnsi = true
+        readerMocker.demand.getCompletionHandler(1) {clch}
         readerMocker.demand.setExpandEvents {}
         readerMocker.demand.addCompleter(2) {}
         readerMocker
@@ -115,8 +119,8 @@ class ShellRunnerTest2 extends GroovyTestCase {
         groovysh.buffers.buffers.add(['Foo { {'])
         groovysh.buffers.select(1)
 
-        MockFor readerMocker = new MockFor(PatchedConsoleReader)
-        readerMocker.demand.setCompletionHandler {}
+        MockFor readerMocker = new MockFor(ConsoleReader)
+        readerMocker.demand.getCompletionHandler {new CandidateListCompletionHandler()}
         readerMocker.demand.setExpandEvents {}
         readerMocker.demand.addCompleter(2) {}
         readerMocker.demand.readLine(1) {'Foo { {'}
diff --git a/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ShellRunnerTestSupport.groovy b/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ShellRunnerTestSupport.groovy
index 3af1e15..a83b70d 100644
--- a/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ShellRunnerTestSupport.groovy
+++ b/subprojects/groovy-groovysh/src/test/groovy/org/codehaus/groovy/tools/shell/ShellRunnerTestSupport.groovy
@@ -64,6 +64,7 @@ abstract class ShellRunnerTestSupport extends GroovyTestCase {
 
         readerStubber = new StubFor(ConsoleReader)
         readerStubber.demand.setExpandEvents {}
+        readerStubber.demand.setCompletionHandler {}
         // adding 2 completers
         readerStubber.demand.addCompleter {}
         readerStubber.demand.printNewline {}
